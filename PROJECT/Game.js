let pScore = 0;
let cScore = 0;

const options = document.querySelectorAll(".options button");
const playerHand = document.querySelector(".player-hand");
const computerHand = document.querySelector(".computer-hand");

const computerOptions = ["rock", "paper", "scissors"];

const btnNextStage1 = document.getElementById("Stage1")
const btnPlayAgain1 = document.getElementById("again1")

const btnNextStage2 = document.getElementById("Stage2")
const btnPlayAgain2 = document.getElementById("again2")

const btnNextStage3 = document.getElementById("Stage3")
const btnPlayAgain3 = document.getElementById("again3")


options.forEach(option => {
    option.addEventListener("click", function() {
        const computerNumber = Math.floor(Math.random() * 3);
        const computerChoice = computerOptions[computerNumber];

        compareHands(this.textContent, computerChoice);
        playerHand.src = `./IMG/${this.textContent}.png`;
        computerHand.src = `./IMG/${computerChoice}.png`;
    });
});

const updateScore = () => {
    const playerScore = document.querySelector(".player-score p");
    const computerScore = document.querySelector(".computer-score p");
    playerScore.textContent = pScore;
    computerScore.textContent = cScore;

    if (pScore === 1) {
        btnNextStage1.style.display = 'block';
        btnPlayAgain1.style.display = 'none';
    } else if (cScore === 1) {
        btnPlayAgain1.style.display = 'block';
        btnNextStage1.style.display = 'none';
    }

    if (pScore === 3) {
        btnNextStage2.style.display = 'block';
        btnPlayAgain2.style.display = 'none';
    } else if (cScore === 3) {
        btnPlayAgain2.style.display = 'block';
        btnNextStage2.style.display = 'none';
    }

    if (pScore === 5) {
        btnNextStage3.style.display = 'block';
        btnPlayAgain3.style.display = 'none';
    } else if (cScore === 5) {
        btnPlayAgain3.style.display = 'block';
        btnNextStage3.style.display = 'none';
    }

};

const compareHands = (playerChoice, computerChoice) => {
    console.log(compareHands)

    const winner = document.querySelector(".winner");
    console.log(winner)

    if (playerChoice === computerChoice) {
        winner.textContent = "TIE";
        return;
    }
    if (playerChoice === "rock") {
        if (computerChoice === "scissors") {
            winner.textContent = "PLAYER WIN";
            pScore++;
            updateScore();
            return;
        } else {
            winner.textContent = "COMPUTER WIN";
            cScore++;
            updateScore();
            return;
        }
    }
    if (playerChoice === "paper") {
        if (computerChoice === "scissors") {
            winner.textContent = "COMPUTER WIN";
            cScore++;
            updateScore();
            return;
        } else {
            winner.textContent = "PLAYER WIN";
            pScore++;
            updateScore();
            return;
        }
    }
    if (playerChoice === "scissors") {
        if (computerChoice === "rock") {
            winner.textContent = "COMPUTER WIN";
            cScore++;
            updateScore();
            return;
        } else {
            winner.textContent = "PLAYER WIN";
            pScore++;
            updateScore();
            return;
        }
    }
};